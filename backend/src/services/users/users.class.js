const { Service } = require('feathers-mongoose')

exports.Users = class Users extends Service {
    create(data, params) {
        const { username, email, password } = data;
        
        const userData = {
			username,
            email,
            password,
            schedules:[]
        }
        
        return super.create(userData, params);
        
    }
    
    update(data, params){
        const { email, password } = data;
    
        let hash;
        
        //Hashes password locally
        LocalStrategy.hashPassword(password)
        .then(result=>{
            hash = result;
        })
        
        const userData = {
            email:email,
            password: hash,
            schedules:data.schedules
        }
        
        return super.update(data.id, userData, params);
    }
}
