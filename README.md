# Course_Scheduling_Proj

@Authors: Matt van Dinther, Carlos Gomez, Jinghao Su

Domain name:
project-frontend.494917.xyz
project-backend.494917.xyz

We ran into a major issue in the deployment stage of this project.  Unfortunately the frontend
is communicating with the backend but not vice-versa.  It's not getting the courses from the backend server, 
possibly due to permission errors with docker switching ownership to 501.  If you run this program locally,
it works perfectly.  It wasn't until we mounted it in the container that it broke.

If you want to see it running locally, you can change feathers.js file in the frontend
to point to either your local host, or AWS instance domain name.  We just ran out of time to figure this last issue out,
but we are thinking it has to do with the way we generated our apps.

We also included a series of images in the course_scheduling_proj folder showing functionality.

Quick start guide:
1. Navigate to the project folder(cd course_scheduling_proj)
2. Run the command(deploy/start-vm.sh)
3. Navigate to the project folder again & run command(deploy/project.sh -d)
*Note: project.sh not working (frontend container keeps crashing), but all docker command works perfect if run separately. 
If 'no permission' is shown in terminal, run 'chmod -R o+w frontend backend' in the project root*

Course Parser: /backend/parsing/scheduleParser.js
Functionality - Finds every table data that met the requirement and write it to courses.js using Node.js file system

Course Selector: /frontend/src/components/SideMenu/CourseSearchForm/CourseSearchForm.js
Functionality - Provided a form with two text input fields for user to enter the course number and course subject

TLS/SSL: /etc/nginx/conf.d
Implemented. All files are in the specified folder.  Decided not to host an html page for the backend.  Since
there is no administrator login or any additional provided functionality, we decided to omit

Intergrated calendar: /frontend/src/components/Calendar/Calendar.js
Implemented using 'react-big-calendar' and localized with English, Spanish and Chinese using the 'momentLocalizer'

DB: /backend/src/mongoose.js
Implemented using MongoDB, to store user's login info

Startup scripts: /deploy
Implemented. All files are ready for Continuous Integration and Deployment

UI/UX:
User tokens stay 24hrs in case of reload, you don't have to log in again
Scheduler is large and easy to navigate.  All buttons have clear purpose and 
forms are labeled to communicate their use.  The modals for logging in/signing up
or sections for loading a schedule look clean overall, and organize the content more
effectively. 

Authentication/oAuth OR Captcha:
Implemented using Google reCAPTCHA V.2 (React wouldn't let us V3)

Accessibility OR Internationalization:
Implemented using react i18n through out the page

CI/CD: /course_scheduling_proj/gitlab-ci.yml
Functionality - Tests application builds and alerts developers if changes failed any unit tests
Gitlab - not accepting our pipeline.  We are getting a lot of permission denied errors in the backend with ownership of
files & folders being passed to 501.  Not sure why this is happening.  Also when we run tests locally, it works so unsure.

Analytics: 
Implemented using Google Analytics

Automated Testing:
Implemented using Jest, tests important functions, isolated 5 main functions for 
frontend to work and tested.  Added additional checks to services backend tests

README.md:
To document Authors, Quick start guide and all necessary pieces for this project
