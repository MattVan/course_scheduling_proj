//This function will just need the right frontend File Path for courses.js
//courses.js should be in the same folder
const getAllCourses = ()=> {
  let courses = [];
  
  require('fs').readFileSync(__dirname + '/courses.js', 'utf-8').split(/\r?\n/).forEach(function(line) {
  
    //throwing out non-standard start-end times
    if(line.trim() != ""){
      let obj = JSON.parse(line);
      
      if(obj.times[3] === " "){
        courses.push(obj);  
      }
    }
  });
  return courses;
};



exports.GetCourses = class GetCourses {
  constructor(options) {
    this.options = options || {};
  }

  async find(params) {
    return [];
  }

  async get(id) {
	  let courses = await getAllCourses();
	  return courses;
  }


  async create(data, params) {
    if (Array.isArray(data)) {
      return Promise.all(data.map((current) => this.create(current, params)));
    }

    return data;
  }

  async update(id, data, params) {
    return data;
  }

  async patch(id, data, params) {
    return data;
  }

  async remove(id, params) {
    return { id };
  }
};
