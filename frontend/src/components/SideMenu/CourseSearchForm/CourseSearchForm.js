import React, { Component } from "react";
import { FaCheck } from "react-icons/fa";
import { IoIosWarning } from "react-icons/io";

class CourseSearchForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showModal: false,
			courseNumber: "",
			courseSubject: "",
		};
		this.handleInputChange = this.handleInputChange.bind(this);
		this.closeModal = this.closeModal.bind(this);
	}

	closeModal() {
		this.setState({
			showModal: false,
		});
	}

	handleInputChange(event) {
		const target = event.target;
		const name = target.name;
		const value = target.value;
		switch (name) {
			case "courseNumber":
				let courseNumberValidation = "is-invalid";
				if (this.validateCourseNumber(value)) {
					courseNumberValidation = "is-valid";
				}
				this.setState({
					courseNumber: value,
					courseNumberValidation: courseNumberValidation,
				});
				break;
			case "courseSubject":
				let courseSubjectValidation = "is-invalid";
				if (this.validateCourseSubject(value)) {
					courseSubjectValidation = "is-valid";
				}
				this.setState({
					courseSubject: value,
					courseSubjectValidation: courseSubjectValidation,
				});
				break;
		}
	}

	validateCourseNumber(value) {
		return value.length === 4 && typeof(value) === 'string' && value > 1000 && value < 6000;
	}

	validateCourseSubject(value) {
		return value.length === 4 && typeof(value) === 'string';
	}

	render() {
		const { t } = this.props;
		console.log(this.props.searchFormFeedback);
		return (
			<div>
				<h4>{t("formLabels.searchFormTitle")}</h4>
				<form onSubmit={this.props.courseSearch}>
					<div className="form-group">
						<label htmlFor="courseNumber">{t("formLabels.courseNumber")}</label>
						<input
							type="number"
							className={[
								"form-control ".concat(this.state.courseNumberValidation),
							]}
							id="courseNumber"
							placeholder={t("formLabels.courseNumberPlaceholder")}
							name={"courseNumber"}
							onChange={this.handleInputChange}
							required
						/>
						<div className="valid-feedback">
							<span>
								<FaCheck /> {t("formLabels.validCourseNumber")}
							</span>
						</div>
						<div className="invalid-feedback">
							<span>
								<IoIosWarning />
								{t("formLabels.invalidCourseNumber")}
							</span>
						</div>
					</div>
					<div className="form-group">
						<label htmlFor="instructor">{t("formLabels.courseSubject")}</label>
						<input
							type="text"
							className={[
								"form-control ".concat(this.state.courseSubjectValidation),
							]}
							id="courseSubject"
							placeholder={t("formLabels.courseSubjectPlaceholder")}
							name={"courseSubject"}
							onChange={this.handleInputChange}
							value={this.state.courseSubject}
							required
						/>
						<div className="valid-feedback">
							<span>
								<FaCheck /> {t("formLabels.validCourseSubject")}
							</span>
						</div>
						<div className="invalid-feedback">
							<span>
								<IoIosWarning />
								{t("formLabels.invalidCourseSubject")}
							</span>
						</div>
					</div>
					<button type="submit" className="btn btn-primary btn-block">
						{t("formLabels.addCourse")}
					</button>
					<p className={["".concat(this.props.searchFormClass)]}>
						{this.props.searchFormFeedback}
					</p>
				</form>
			</div>
		);
	}
}

export default CourseSearchForm;
