import React from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";

import CourseSearchForm from "./CourseSearchForm/CourseSearchForm.js";
import CurrentSchedule from "./CurrentSchedule/CurrentSchedule.js";
import UserSchedules from "./UserSchedules/UserSchedules.js";

const SideMenu = (props) => {
	const { t } = props;
	const { userService } = props;
	return (
		<Tabs>
			<TabList>
				<Tab>{t("tabs.scheduleBuilder")}</Tab>
				<Tab>{t("tabs.userSchedules")}</Tab>
			</TabList>

			<TabPanel>
				<CourseSearchForm
					currentSchedule={props.currentSchedule}
					courseSearch={props.courseSearch}
					searchFormFeedback={props.searchFormFeedback}
					searchFormClass={props.searchFormClass}
					t={t}
				/>
				<CurrentSchedule
					t={t}
					user={props.user}
					currentSchedule={props.currentSchedule}
					nonConflictSchedules={props.nonConflictSchedules}
					currentIndex={props.currentIndex}
					previousIndex={props.previousIndex}
					nextIndex={props.nextIndex}
					saveSchedule={props.saveSchedule}
					deleteCourse={props.deleteCourse}
				/>
			</TabPanel>
			<TabPanel>
				<UserSchedules
					user={props.user}
					userSchedules={props.userSchedules}
					t={t}
					userService={userService}
					login={props.login}
					logout={props.logout}
					loadSchedule={props.loadSchedule}
					deleteSchedule={props.deleteSchedule}
				/>
			</TabPanel>
		</Tabs>
	);
};

export default SideMenu;
