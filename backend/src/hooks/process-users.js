// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
const querystring = require("querystring");
require("dotenv").config();

const axios = require("axios");

module.exports = (options = {}) => {
  return async (context) => {
    const { data } = context;

    console.log("Processing user");
    console.table(data);

    const response = await axios.post(
      "https://www.google.com/recaptcha/api/siteverify",
      querystring.stringify({
        secret: process.env.RECAPTCHA_KEY,
        response: data.token,
      })
    );

    // if the response fails or the score is too low, throw an error
    if (!response.data.success || response.data.score < 0.7) {
      throw new Error("reCAPTCHA failed");
    }

    let num = false;
    let upperChar = false;
    let nonChar = false;

    let charReg = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    let upperCharReg = new RegExp("[A-Z]");
    if (charReg.test(data.password)) {
      nonChar = true;
    }

    if (data.password.match(upperCharReg)) {
      upperChar = true;
    }
    for (let i = 0; i < data.password.length; i++) {
      if (!/\D/.test(data.password.charAt(i))) num = true;
    }
    if(!num || !upperChar || !nonChar || !data.password.length > 8){
        throw new Error("Bad password, must have at least Upper Case letter, one Special Character, and one Number");
	}

    console.log("User processed successfully");

    return context;
  };
};
