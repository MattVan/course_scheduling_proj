import React, { Component } from "react";

import Recaptcha from "react-recaptcha";
import client from "../../../feathers";

import { FaCheck } from "react-icons/fa";

import { IoIosWarning } from "react-icons/io";

export default class SignUp extends Component {
	constructor(props) {
		super(props);
		this.state = {
			feedback: "",
			feedbackClass: "",
			userService: {},
			captchaResponse: "",
			recaptchaInstance: "",
			email: "",
			emailValidation: "",
			username: "",
			password: "",
			passwordValidation: "",
		};

		this.signUp = this.signUp.bind(this);
		this.handleInputChange = this.handleInputChange.bind(this);
		this.verifyCallback = this.verifyCallback.bind(this);
	}

	componentDidMount() {
		const { userService } = this.props;

		this.setState({ userService });
	}

	handleInputChange(event) {
		const target = event.target;
		const value = target.value;
		const name = target.name;

		switch (name) {
			case "email":
				let emailValidation = "is-invalid";
				if (this.verifyEmail(value)) {
					emailValidation = "is-valid";
				}
				this.setState({
					email: value,
					emailValidation: emailValidation,
				});
				break;
			case "password":
				let passwordValidation = "is-invalid";
				if (this.verifyPassword(value)) {
					passwordValidation = "is-valid";
				}
				this.setState({
					password: value,
					passwordValidation: passwordValidation,
				});
				break;
			case "username":
				this.setState({
					username: value,
				});
				break;
			default:
				break;
		}
	}

	verifyEmail(value) {
		var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(value);
	}

	verifyPassword(value) {
		let num = false;
		let upperChar = false;
		let nonChar = false;
		let charReg = /[ `!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?~]/;
		let upperCharReg = new RegExp("[A-Z]");

		// validate against special characters
		if (charReg.test(value)) {
			nonChar = true;
		}

		// validate against upper case characters
		if (value.match(upperCharReg)) {
			upperChar = true;
		}
		// validate for numerical digits
		for (let i = 0; i < value.length; i++) {
			if (!/\D/.test(value.charAt(i))) num = true;
		}
		return num && upperChar && nonChar && value.length > 8;
	}

	verifyCallback(response) {
		const captchaResponse = response;
		this.setState({
			captchaResponse: captchaResponse,
		});
	}

	signUp(event) {
		event.preventDefault();
		const email = event.target.email.value;
		const username = event.target.username.value;
		const password = event.target.password.value;

		const userService = this.state.userService;
		const captchaResponse = this.state.captchaResponse;

		userService
			.create({
				username: username,
				email: email,
				password: password,
				token: captchaResponse,
			})
			.then((response) => {
				client.authenticate({
					strategy: "local",
					email: email,
					password: password,
				});
				this.setState({
					feedback: "Registration successful. Please log in",
					feedbackClass: "alert alert-success",
				});
			})
			.catch((error) => {
				this.setState({
					feedback: "Error - " + error.message,
					feedbackClass: "alert alert-danger",
				});
				this.recaptchaInstance.reset();
			});
	}

	render() {
		const { t } = this.props;
		const feedback = this.state.feedback;
		return (
			<div>
				<form onSubmit={this.signUp}>
					<div className="form-group">
						<label htmlFor="email">
							{t("authentication.modal.tabs.signUpForm.email")}:
						</label>
						<input
							type="email"
							name="email"
							className={["form-control ".concat(this.state.emailValidation)]}
							id="email"
							placeholder={t(
								"authentication.modal.tabs.signUpForm.emailPlaceholder"
							)}
							onChange={this.handleInputChange}
							value={this.state.email}
							required
						/>
						<div className="valid-feedback">
							<span>
								<FaCheck />{" "}
								{t("authentication.modal.tabs.signUpForm.validEmail")}
							</span>
						</div>
						<div className="invalid-feedback">
							<span>
								<IoIosWarning />
								{t("authentication.modal.tabs.signUpForm.invalidEmail")}
							</span>
						</div>
					</div>
					<div className="form-group">
						<label htmlFor="username">
							{t("authentication.modal.tabs.signUpForm.username")}:
						</label>
						<input
							type="text"
							name="username"
							className={"form-control"}
							id="username"
							placeholder={t(
								"authentication.modal.tabs.signUpForm.usernamePlaceholder"
							)}
							onChange={this.handleInputChange}
							value={this.state.username}
							required
						/>
					</div>
					<div className="form-group">
						<label htmlFor="password">
							{t("authentication.modal.tabs.signUpForm.password")}
						</label>
						<input
							type="password"
							name="password"
							className={[
								"form-control ".concat(this.state.passwordValidation),
							]}
							id="password"
							placeholder={t(
								"authentication.modal.tabs.signUpForm.passwordPlaceholder"
							)}
							onChange={this.handleInputChange}
							value={this.state.password}
							required
						/>
						<div className="valid-feedback">
							<span>
								<FaCheck />{" "}
								{t("authentication.modal.tabs.signUpForm.validPassword")}
							</span>
						</div>
						<div className="invalid-feedback">
							<span>
								<IoIosWarning />
								{t("authentication.modal.tabs.signUpForm.invalidPassword")}
							</span>
						</div>
						<small>
							{t("authentication.modal.tabs.signUpForm.passwordHelper")}
						</small>
					</div>
					<Recaptcha
						sitekey={process.env.REACT_APP_CAPTCHA_KEY}
						verifyCallback={this.verifyCallback}
						ref={(e) => (this.recaptchaInstance = e)}
						size="normal"
					/>
					<button type="submit" className="btn btn-primary btn-block">
						{t("authentication.modal.tabs.signUpForm.submitButton")}
					</button>
				</form>
				<div>
					<p className={this.state.feedbackClass}>{feedback}</p>
				</div>
			</div>
		);
	}
}
