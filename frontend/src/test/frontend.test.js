// const findCourses = require("../utils/Course");
// const nonConflictingCourses = require("../utils/Course");
// const createCalendarObj = require("../utils/Course");
// const deleteCrse = require("../utils/Course");

import {
	findCourses,
	nonConflictingCourses,
	createCalendarObj,
	deleteCrse
} from "../utils/Course";

const getAllCourses = ()=> {
  let courses = [];
  
  require('fs').readFileSync('/home/ec2-user/environment/course_scheduling_proj/backend/src/services/get-courses/courses.js', 'utf-8').split(/\r?\n/).forEach(function(line) {
  
    //throwing out non-standard start-end times
    if(line.trim() != ""){
      let obj = JSON.parse(line);
      
      if(obj.times[3] === " "){
        courses.push(obj);  
      }
    }
  });
  return courses;
};

let courses = [];

courses = getAllCourses();

describe("Init Courses", ()=>{
	//unsure what the actual length is right now
	test("Courses right length", ()=>{
		expect(courses).toHaveLength(1271);
	});
});

describe("Add Course to Schedule", ()=>{
	let currentSchedule = [];
	
	describe("Good values", ()=>{
		
		test("1 value", ()=>{
			currentSchedule = [];
			
			currentSchedule.push(findCourses("CPSC", "2650", currentSchedule, courses));
			expect(currentSchedule).toHaveLength(1);
			expect(currentSchedule[0]).toHaveLength(1);
		});
		
		test("3 values", ()=>{
			currentSchedule = [];
			currentSchedule.push(findCourses("CPSC", "1150", currentSchedule, courses));
			currentSchedule.push(findCourses("ENGL", "1127", currentSchedule, courses));
			currentSchedule.push(findCourses("CPSC", "1030", currentSchedule, courses));
			expect(currentSchedule).toHaveLength(3);
		});
		
		test("Max Values(5), Lower case subj", ()=>{
			currentSchedule = [];
			
			currentSchedule.push(findCourses("CPSC", "1150", currentSchedule, courses));
			currentSchedule.push(findCourses("engl", "1127", currentSchedule, courses));
			currentSchedule.push(findCourses("cpsc", "1030", currentSchedule, courses));
			currentSchedule.push(findCourses("CPSC", "1045", currentSchedule, courses));
			currentSchedule.push(findCourses("CPSC", "2650", currentSchedule, courses));
			expect(currentSchedule).toHaveLength(5);
		});
	});
	
	describe("Too many courses", ()=>{
		
		test("Add 1 too many", ()=>{
			currentSchedule = [];
			
			currentSchedule.push(findCourses("CPSC", "1150", currentSchedule, courses));
			currentSchedule.push(findCourses("engl", "1127", currentSchedule, courses));
			currentSchedule.push(findCourses("cpsc", "1030", currentSchedule, courses));
			currentSchedule.push(findCourses("CPSC", "1045", currentSchedule, courses));
			currentSchedule.push(findCourses("CPSC", "2650", currentSchedule, courses));
			if(findCourses("CPSC", "1030", currentSchedule, courses) != undefined){
				currentSchedule.push(findCourses("CPSC", "1030", currentSchedule, courses));	
			}

			expect(currentSchedule).toHaveLength(5);
		});
		
		test("Add 3 too many", ()=>{
			currentSchedule = [];
			
			currentSchedule.push(findCourses("CPSC", "1150", currentSchedule, courses));
			currentSchedule.push(findCourses("engl", "1127", currentSchedule, courses));
			currentSchedule.push(findCourses("cpsc", "1030", currentSchedule, courses));
			currentSchedule.push(findCourses("CPSC", "1045", currentSchedule, courses));
			currentSchedule.push(findCourses("CPSC", "2650", currentSchedule, courses));
			
			if(findCourses("CPSC", "1030", currentSchedule, courses) != undefined){
				currentSchedule.push(findCourses("CPSC", "1030", currentSchedule, courses));	
			}
			if(findCourses("CPSC", "2600", currentSchedule, courses) != undefined){
				currentSchedule.push(findCourses("CPSC", "2600", currentSchedule, courses));
			}
			if(findCourses("ENGL", "1129", currentSchedule, courses) != undefined){
				currentSchedule.push(findCourses("ENGL", "1129", currentSchedule, courses));	
			}
			expect(currentSchedule).toHaveLength(5);
		});
	});
	
	describe("Bad Values", ()=>{
		
		beforeAll(()=>{
			currentSchedule = [];
			currentSchedule.push(findCourses("CPSC", "1150", currentSchedule, courses));
			currentSchedule.push(findCourses("ENGL", "1127", currentSchedule, courses));
		});

		test("Course subj not there", ()=>{
			
			let test1 = findCourses("CPSCP", "1150", currentSchedule, courses);
			expect(test1).toHaveLength(0);
		});
		
		test("Course # not there", ()=>{
			let test1 = findCourses("CPSC", "115051", currentSchedule, courses);
			expect(test1).toHaveLength(0);
		});
		
		test("Course already there", ()=>{
			if(findCourses("CPSC", "1150", currentSchedule, courses) != undefined){
				currentSchedule.push(findCourses("CPSC", "1150", currentSchedule, courses));	
			}
			expect(currentSchedule).toHaveLength(2);
		});
		
		test("Course not there, not in first location of currentSchedule", ()=>{
			if(findCourses("ENGL","1127", currentSchedule, courses) != undefined){
				currentSchedule.push(findCourses("ENGL","1127", currentSchedule, courses));	
			}
			
			
			expect(currentSchedule).toHaveLength(2);
		});
	});
});

describe("Delete Course", ()=>{
	let currentSchedule = [];
	let output = [];
	currentSchedule.push(findCourses("CPSC", "1150", currentSchedule, courses));
	currentSchedule.push(findCourses("engl", "1127", currentSchedule, courses));
	currentSchedule.push(findCourses("cpsc", "1030", currentSchedule, courses));
	currentSchedule.push(findCourses("CPSC", "1045", currentSchedule, courses));
	currentSchedule.push(findCourses("CPSC", "2650", currentSchedule, courses));
	
	// console.log(currentSchedule.length);
	
	describe("Delete /w good values", ()=>{
		test("Delete one course", ()=>{
			output = deleteCrse(currentSchedule, "1150", "CPSC");
			expect(output).toHaveLength(4);
		});
		
		test("Delete 3 courses in order", ()=>{
			output = deleteCrse(currentSchedule, "1150", "CPSC");
			output = deleteCrse(output, "1127", "ENGL");
			output = deleteCrse(output, "1030", "CPSC");
			expect(output).toHaveLength(2);
		});
		
		test("Delete all courses in order", ()=>{
			output = deleteCrse(currentSchedule, "1150", "CPSC");
			output = deleteCrse(output, "1127", "ENGL");
			output = deleteCrse(output, "1030", "CPSC");
			output = deleteCrse(output, "1045", "CPSC");
			output = deleteCrse(output, "2650", "CPSC");
			expect(output).toHaveLength(0);
			output = [];
		});
		
		test("Delete all courses not in order", ()=>{
			output = deleteCrse(currentSchedule, "1150", "CPSC");
			output = deleteCrse(output, "1030", "CPSC");
			output = deleteCrse(output, "2650", "CPSC");
			output = deleteCrse(output, "1045", "CPSC");
			output = deleteCrse(output, "1127", "ENGL");
			expect(output).toHaveLength(0);
			output = [];
		});
	});
	describe("Bad values", ()=>{
		test("Bad subj", ()=>{
			output = deleteCrse(currentSchedule, "1150", "CPSCCS");
			expect(output).toHaveLength(5);
		});	
		
		test("Bad crse no", ()=>{
			output = deleteCrse(currentSchedule, "115120", "CPSC");
			expect(output).toHaveLength(5);
		});
		
		test("Empty Courses Array", ()=>{
			let testArr= [];
			output = deleteCrse(testArr, "1150", "CPSC");
			expect(output).toHaveLength(0);
		});
	});
	
});

describe("No-conflict Schedule & createCalendarObj", ()=>{
	let output = [];
	let currentSchedule = [];
	
	describe("Bad-ish value", ()=>{
		test("Empty Array", ()=>{
			output = nonConflictingCourses([]);
			expect(output).toHaveLength(0);
			
			output = createCalendarObj(output);
			
			expect(output).toHaveLength(0);
			
		});
		
		
	});
	
	describe("Good Values", ()=>{
		currentSchedule = [];
		test("One Course", ()=>{
			currentSchedule.push(findCourses("CPSC", "1150", currentSchedule, courses));
			output = nonConflictingCourses(currentSchedule);
			
			expect(output).toHaveLength(5);
			
			let testArr = createCalendarObj(output);
			expect(testArr).toHaveLength(5);
		});
		
		test("3 Courses", ()=>{
			currentSchedule = [];
			
			currentSchedule.push(findCourses("CPSC", "1181", currentSchedule, courses));
			currentSchedule.push(findCourses("ENGL", "1127", currentSchedule, courses));
			currentSchedule.push(findCourses("CPSC", "1050", currentSchedule, courses));
			
			output = nonConflictingCourses(currentSchedule);
			
			expect(output).toHaveLength(616);
			
			let testArr = createCalendarObj(output);
			expect(testArr).toHaveLength(616);
		});
		
		test("5 Courses", ()=>{
			currentSchedule = [];
			
			currentSchedule.push(findCourses("CPSC", "1181", currentSchedule, courses));
			currentSchedule.push(findCourses("ENGL", "1127", currentSchedule, courses));
			currentSchedule.push(findCourses("CPSC", "1050", currentSchedule, courses));
			currentSchedule.push(findCourses("CPSC", "2650", currentSchedule, courses));
			currentSchedule.push(findCourses("CPSC", "2221", currentSchedule, courses));
			
			output = nonConflictingCourses(currentSchedule);
			
			let testArr = createCalendarObj(output);
			
			expect(testArr).toHaveLength(533);
			expect(output).toHaveLength(533);
		});
	});
	
});
