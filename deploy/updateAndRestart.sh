#!/bin/bash

docker stop backend

docker stop frontend

# Delete the old repo
rm -rf /home/ec2-user/course_scheduling_proj

# any future command that fails will exit the script
set -e

# BE SURE TO UPDATE THE FOLLOWING LINE WITH THE URL FOR YOUR REPO
git clone https://gitlab.com/MattVan/course_scheduling_proj.git

cd /home/ec2-user/course_scheduling_proj

git checkout master

# run the node app in a container
deploy/project.sh -d