import React from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";

import MyModal from "../../UI/Modal";
import LogIn from "../LogIn/LogIn";
import SignUp from "../SignUp/SignUp";

const AuthenticationModal = props => {
	const { t } = props;
	const { userService } = props;
	return (
		<MyModal
			show={props.showModal}
			size={"lg"}
			title={t("authentication.modal.title")}
			close={props.closeModal}
			centered
		>
			<Tabs>
				<TabList>
					<Tab>{t("authentication.modal.tabs.logInTitle")}</Tab>
					<Tab>{t("authentication.modal.tabs.signUpTitle")}</Tab>
				</TabList>
				<TabPanel>
					<LogIn 
						login={props.login} 
						t={t} 
						feedback={props.feedback}
					/>
				</TabPanel>
				<TabPanel>
					<SignUp 
						onSignUp={props.onSignUp} 
						t={t} 
						feedback={props.feedback}
						userService={userService}
					/>
				</TabPanel>
			</Tabs>
		</MyModal>
	);
};

export default AuthenticationModal;
