import React from 'react';

import Modal from 'react-bootstrap/Modal';


const MyModal = props => {
    return(
        <Modal 
            show={props.show}
            size={props.size}
            centered
            onHide={props.close}
        >        
            <Modal.Header closeButton> 
                <Modal.Title id="modal"> 
                    {props.title}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {props.children}
            </Modal.Body>
        </Modal>
    ); 
}

export default MyModal;