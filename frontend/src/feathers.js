import feathers from '@feathersjs/client';
import rest from '@feathersjs/rest-client';

const client = feathers();

const backend = "project-backend.494917.xyz";
const backendLocal = "localhost:3030";
const backendSSL = `https://${backend}`;

const restClient = rest(`http://${backendLocal}`);

client.configure(restClient.fetch(window.fetch));
client.configure(feathers.authentication({
	storage: window.localStorage
}))

export default client;
