// Initializes the `getCourses` service on path `/get-courses`
const { GetCourses } = require('./get-courses.class');
const hooks = require('./get-courses.hooks');

module.exports = function (app) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/get-courses', new GetCourses(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('get-courses');

  service.hooks(hooks);
};
