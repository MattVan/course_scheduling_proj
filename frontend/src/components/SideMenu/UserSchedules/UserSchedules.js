import React from "react";

import Authentication from "../../Authentication/Authentication";

import Card from "react-bootstrap/Card";

const UserSchedules = (props) => {
	const { t } = props;
	const { userService } = props;

	// Check for existing user, otherwise return Authentication component
	if (!props.user) {
		return (
			<Authentication t={t} userService={userService} login={props.login} />
		);
	}
	// Check if the user has zero schedules saved
	if (props.userSchedules.length === 0) {
		console.log(props.user);
		return (
			<div>
				<h4 className="">
					{t("userSchedules.welcome")}, {props.user.username}.{" "}
				</h4>
				<p>{t("userSchedules.noSchedules")}</p>
				<p>{t("userSchedules.noSchedules2")}</p>
				<button
					onClick={props.logout}
					className="btn btn-secondary btn-block align-self-end mt-5"
				>
					{t("userSchedules.logout")}
				</button>
			</div>
		);
	}

	// Sort the user schedules form newest to oldest
	let userSchedules = props.userSchedules.sort((scheduleA, scheduleB) => {
		return scheduleA.updatedAt < scheduleB.updatedAt;
	});
	//Map schedules into card objects
	userSchedules = props.userSchedules.map((schedule) => {
		return (
			<Card key={schedule._id} style={{ width: "100%", marginBottom: "3px" }}>
				<Card.Body>
					<Card.Title>
						<strong>{t("userSchedules.scheduleName")}:</strong>{" "}
						{schedule.scheduleName}
					</Card.Title>
					<Card.Subtitle>
						<strong>{t("userSchedules.lastUpdated")}: </strong>{" "}
						{schedule.updatedAt.substring(0, 10) +
							" " +
							schedule.updatedAt.substring(11, 19)}
					</Card.Subtitle>
					<button
						onClick={() => {
							props.loadSchedule(schedule._id);
						}}
						className="btn btn-primary m-2"
					>
						{t("userSchedules.load")}
					</button>
					<button
						onClick={() => {
							props.deleteSchedule(schedule._id);
						}}
						className="btn btn-danger m-2"
					>
						{t("userSchedules.delete")}
					</button>
				</Card.Body>
			</Card>
		);
	});

	return (
		<div className="">
			<h5 className="text-left pt-3 ml-0">
				{t("userSchedules.logout")}, <strong>{props.user.username}</strong>.{" "}
				{t("userSchedules.userSchedulesListing")}:
			</h5>
			<div className="overflow-auto sidemenu d-flex flex-column justify-content-center align-items-center">
				{userSchedules}
			</div>
			<button
				onClick={props.logout}
				className="btn btn-secondary btn-block align-self-end mt-5"
			>
				{t("userSchedules.logout")}
			</button>
		</div>
	);
};

export default UserSchedules;
