const { authenticate } = require('@feathersjs/authentication').hooks

const {
  hashPassword, protect
} = require('@feathersjs/authentication-local').hooks

const processUsers = require('../../hooks/process-users');

const validateUser = require('../../hooks/validate-user');

module.exports = {
  before: {
    all: [],
    find: [authenticate('jwt')],
    get: [authenticate('jwt'), validateUser()],
    create: [hashPassword('password'), processUsers()],
    update: [hashPassword('password'), authenticate('jwt'), processUsers()],
    patch: [hashPassword('password'), authenticate('jwt'), processUsers()],
    remove: [authenticate('jwt')]
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect('password')
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
}
