const app = require('../../src/app');

describe('\'schedules\' service', () => {
  test("Get sched", ()=>{
    const service = app.service('schedules');
  
    service.find({query:{
      userId:"5e90cd722cfb0d3169cfdd1a"
    }})
    .then(result=>{
      expect(result.data).toHaveLength(1);
    })
    .catch(error=>console.log(error));
  });
  
  it('registered the service', () => {
    const service = app.service('schedules');
    expect(service).toBeTruthy();
  });
  
});
