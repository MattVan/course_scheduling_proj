import React, { Component } from "react";
import AuthenticationModal from "./AuthenticationModal/AuthenticationModal";

import { toast } from 'react-toastify';

export default class Authentication extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showModal: false,
			userService: null
		};

		this.showModal = this.showModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.signUp = this.signUp.bind(this);
	}

	componentDidMount(){
		const { userService } = this.props;
		this.setState({userService});
	}

	showModal() {
		this.setState({
			showModal: true
		});
	}

	closeModal() {
		this.setState({
			showModal: false
		});
	}

	signUp(event) {
		event.preventDefault();
		let email = event.target.email.value;
		let username = event.target.username.value;
		let password = event.target.password.value;
		console.log(username);

		const userService = this.state.userService;
		userService
			.create({
				username: username,
				email: email,
				password: password,
				schedules: {}
			})
			.then(response => {
				console.table(response);
				toast.success('Success: Registration complete. Please log in.');
				this.setState({
					feedback: 'Registration successful. Please log in' 
				})
			})
			.catch(error => {
				console.log("error response");
				console.log(error);
				toast.error('Error: ' + error);
				this.setState({
					feedback: error
				})
			});
		// this.closeModal();
	}

	render() {
		const { t } = this.props;
		const { userService } = this.props;
		return (
			<div>
				<AuthenticationModal
					showModal={this.state.showModal}
					closeModal={this.closeModal}
					login={this.props.login}
					onSignUp={this.signUp}
					t={t}
					userService={userService}
				/>
				<div>
					<p>{t("authentication.prompt")}</p>
					<button
						onClick={this.showModal}
						className="btn btn-secondary btn-block"
					>
						{t("authentication.button")}
					</button>
				</div>
			</div>
		);
	}
}
