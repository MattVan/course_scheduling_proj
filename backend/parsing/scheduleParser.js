const axios = require('axios');
const htmlparser2 = require("htmlparser2");
const fs = require('fs');
var wstream = fs.createWriteStream('../src/services/get-courses/courses.js');

var rowArray = [],
    resultArray = [],
    complCourses = [];

let parseTimes = (arr) => {
    let timesArray = [];

    for (let i = 0; i < arr.length; i++) {
        timesArray.push(arr[i][12]);
        timesArray.push(arr[i][13]);
        timesArray.push(arr[i][14]);
        timesArray.push(arr[i][15]);
        timesArray.push(arr[i][16]);
        timesArray.push(arr[i][17]);
        timesArray.push(arr[i][18]);

    }

    return timesArray;

};

let newCourse = (arr) => {
    let newCourse = {
        rp: arr[0][0],
        seats: arr[0][1],
        wait: arr[0][2],
        sel: arr[0][3],
        crn: arr[0][4],
        subj: arr[0][5],
        crse: arr[0][6],
        sec: arr[0][7],
        cred: arr[0][8],
        title: arr[0][9],
        fees: arr[0][10],
        rpt: arr[0][11],
        times: parseTimes(arr)
    };
    return newCourse;
};

var checktext = false;
var start = false;
var rowStart = false;
var startAfter = false;

const parser = new htmlparser2.Parser({
    onopentag(name, attributes) {
        //found the correct table, start looking at rows
        if (name === 'table' && attributes.class === 'dataentrytable') {
            start = true;
        }
        //on opening tr tag, start checking td's after
        if (start && name === 'tr' && attributes.bgcolor != "#FFFFCC") {
            startAfter = true;
        }

        //first data location in row & those after
        if (name === 'td') {
            if (rowStart) {
                checktext = true;
            }
        }
    },
    ontext(text) {
        //start means inside table
        //check text means we are on a <td>
        if (start && checktext) {
            rowArray.push(text.trim().replace(/&nbsp;/gi, ' '));

        }
        //starts pushing td values in starting next data
        if (startAfter) {
            rowStart = true;
        }
    },
    onclosetag(name) {
        //still on table, but that row has finished
        if (start && name === 'tr') {
            //next 3 lines: resetting for next tr
            startAfter = false;
            rowStart = false;
            checktext = false;

            //if length != 19, we don't have a valid row
            if (rowArray.length === 19) {

                if (resultArray.length === 0) {
                    resultArray.push(rowArray);
                    rowArray = [];
                }
                else if (rowArray[4] === ' ') {
                    resultArray.push(rowArray);
                    rowArray = [];
                }
                else if (rowArray[4] !== ' ') {
                    complCourses.push(newCourse(resultArray));
                    resultArray = [];
                    resultArray.push(rowArray);
                    rowArray = [];
                }

            }
            else {
                // console.log(rowArray);
                rowArray = [];
            }
        }

        if (name === 'td') {
            checktext = false;
        }

        //end of correct table, stop parsing
        if (name === 'table' && start) {
            start = false;
            //testing data
        }
    }
});

axios.get('https://full-stack-web-development-ii.koehler.ca/langara-schedule-202010.html')
    .then(function(response) {
        parser.write(
            response.data
        );


        parser.end();
        for (let i = 0; i < complCourses.length; i++) {
            wstream.write(JSON.stringify(complCourses[i]));
            wstream.write('\n');

        }

        wstream.end();

    })
    .catch(function(error) {
        // handle error
        console.log(error);
    });

exports.complCourses;
