//this function searches through the courses array
//@params -
//subj (The subject in 4 letter format IE CPSC)
//crse - the 4 digit course number
//searchedCourses - The array of searchedCourses already searched for
//courses - the array of courses to find the courses from, in the application
//should be passed through as this.state.courses or some variation of that
//@returns - an array of courses matching the subj & course
exports.findCourses = (subj, crse, searchedCourses, courses) => {
	let result = [];

	//need to check if array is of length 5 already, and if course has already been searched for
	//Capping at 5 courses, because you can't have 6 without authorization anyway

	if (searchedCourses.length === 5) {
		return;
	} else if (searchedCourses.length > 0) {
		for (let i = 0; i < searchedCourses.length; i++) {
			if (
				searchedCourses[i][0].crse === crse.trim() &&
				searchedCourses[i][0].subj === subj.toUpperCase().trim()
			) {
				return;
			}
		}
	}

	courses.forEach((course) => {
		// Finds matching subj & crse then push course into state
		if (
			course.subj === subj.toUpperCase().trim() &&
			course.crse === crse.trim()
		) {
			//pulling each course pertaining to the course name and 4 dig code

			result.push(course);
		}
	});
	if(result.length != 0){
            return result;
        } else{
            return;
        }
};

//Finds non-conflicting course schedules
//@params -
//searchedCourses - the filtered list of courses that have been searched for
//@ returns - a 2d Array of non-conflicting courses
exports.nonConflictingCourses = (searchedCourses) => {
	//if the array of searchedCourses is of length 0 or 1, we just return that
	//since length 0 or 1 can't have conflicts
	// if (searchedCourses.length === 0 || searchedCourses.length === 1) {
	//     return pullSections(searchedCourses);
	// }
	//helper function to get the required information from each section
	const pullSections = (arr) => {
		let result = [];

		let subj, crse, startDate, endDate, descr, seats, wait, crn, sec, cred;
		//{"rp":"R","seats":"9","wait":" ","sel":" ","crn":"11663","subj":"ABST","crse":"1106","sec":"001","cred":"0.00","title":"Aboriginal Studies Seminar","fees":" ","rpt":"-","times":["Seminar","-T-R---","1330-1420"," "," ","A167","Justin Wilson"]}
		for (let i = 0; i < arr.length; i++) {
			subj = arr[i].subj;
			crse = arr[i].crse;
			descr = arr[i].title;
			seats = arr[i].seats;
			wait = arr[i].wait;
			crn = arr[i].crn;
			sec = arr[i].sec;
			cred = arr[i].cred;
			let sections = [];

			let _days, _times, _instr, _room, _type;

			for (let j = 0; j < arr[i].times.length; j++) {
				if (j % 7 === 0) {
					_type = arr[i].times[j];
				}

				if (j % 7 === 1) {
					_days = arr[i].times[j];
				}

				if (j % 7 === 2) {
					_times = arr[i].times[j].split("-");
				}

				if (j % 7 === 3) {
					startDate = arr[i].times[j];
				}

				if (j % 7 === 4) {
					endDate = arr[i].times[j];
				}

				if (j % 7 === 5) {
					_room = arr[i].times[j];
				}

				if (j % 7 === 6) {
					_instr = arr[i].times[j];

					let newSection = {
						seats: seats,
						wait: wait,
						crn: crn,
						sec: sec,
						cred: cred,
						instr: _instr,
						room: _room,
						type: _type,
						subj: subj,
						crse: crse,
						days: _days,
						startTime: _times[0],
						endTime: _times[1],
						startDate: startDate,
						endDate: endDate,
						descr: descr,
					};

					sections.push(newSection);
				}
			}
			result.push(sections);
			sections = [];
		}
		// console.log("Line 116",result);
		return result;
	};

	//packs schedules together to maintain array length & consistency
	const pack = (arr1, arr2) => {
		let result = [];

		for (let i = 0; i < arr1.length; i++) {
			result.push(arr1[i]);
		}
		for (let i = 0; i < arr2.length; i++) {
			result.push(arr2[i]);
		}
		return result;
	};

	//another helper function identifying conflicts and returning
	//the array of non-conflicting courses
	const conflicts = (arr1, arr2) => {
		let result = [];

		//case case to add initial
		if (arr1.length === 0) {
			return arr2;
			//return this arr 1 = [];
		}
		//1050 = [ i0:[{k0} , {k1} ] , i1:[ {k0}, {k1} ] ]
		//1127 = [ j0:[{n0} , {n1} ] , j1:[ {n0}, {n1} ] ]

		for (let i = 0; i < arr1.length; i++) {
			//deals with course 1 array arr[i]

			for (let j = 0; j < arr2.length; j++) {
				//deals with course2 arr[j]
				let conflict = false;

				label1: for (let k = 0; k < arr1[i].length; k++) {
					//deals with course1 arr[i][k]

					for (let n = 0; n < arr2[j].length; n++) {
						//deals with course2 arr[j][n]
						let start1 = arr1[i][k].startTime;
						let end1 = arr1[i][k].endTime;
						let start2 = arr2[j][n].startTime;
						let end2 = arr2[j][n].endTime;

						for (let x = 0; x < arr1[i][k].days.length; x++) {
							for (let z = 0; z < arr2[j][n].days.length; z++) {
								if (
									arr1[i][k].days.charAt(x) === arr2[j][n].days.charAt(z) &&
									arr1[i][k].days.charAt(x) !== "-"
								) {
									if (start1 >= start2 && start1 < end2) {
										//start time conflicts
										conflict = true;
										break label1;
									} else if (end1 >= start2 && end1 <= end2) {
										//end time conflicts
										conflict = true;
										break label1;
									}
								}
							}
						}
					}
				} //end label 1

				if (!conflict) {
					result.push(pack(arr1[i], arr2[j]));
				}
			}
		}
		if(result.length != 0){
            return result;
        } else{
            return arr1;
        }
	};

	let output = [];

	//Final loop to find all conflicts from the searchedCourses array
	for (let i = 0; i < searchedCourses.length; i++) {
		if (conflicts(output, pullSections(searchedCourses[i]).length !== 0)) {
			output = conflicts(output, pullSections(searchedCourses[i]));
		}
	}

	return output;
};

function findDays(days) {
	let week = [5, 6, 7, 8, 9, 10, 11];

	let weekDays = ["su", "m", "t", "w", "r", "f", "s"];

	let result = [];

	let splitDays = days.split("-");

	for (let i = 0; i < splitDays.length; i++) {
		if (splitDays[i] !== "-") {
			let index = weekDays.indexOf(splitDays[i].toLowerCase());
			result.push(week[index]);
		}
	}

	return result;
}

function createWeek(arr) {
	let result = [];
	let id = 1;
	for (let i = 0; i < arr.length; i++) {
		if (
			arr[i].type !== "WWW" &&
			arr[i].room !== "WWW" &&
			arr[i].room !== "OFFC"
		) {
			let days = findDays(arr[i].days);

			for (let j = 0; j < days.length; j++) {
				// console.log("arr[i].startTime.substring(0,2): "+arr[i].startTime.substring(0,2));
				// console.log("arr[i].startTime.substring(2): "+arr[i].startTime.substring(2));
				// console.log("days[j]:" + days[j]);
				if (!days[j]) {
					continue;
				}
				let startDate = new Date(
					2020,
					0,
					days[j],
					arr[i].startTime.substring(0, 2),
					arr[i].startTime.substring(2)
				);
				let endDate = new Date(
					2020,
					0,
					days[j],
					arr[i].endTime.substring(0, 2),
					arr[i].endTime.substring(2)
				);

				let calendarObj = {
					id: id,
					title: `${arr[i].subj} ${arr[i].crse}`,
					start: startDate,
					end: endDate,
					desc: arr[i].descr,
				};
				result.push(calendarObj);
				id++;
			}
		}
	}
	return result;
}

exports.createCalendarObj = (arr) => {
	let result = [];

	for (let i = 0; i < arr.length; i++) {
		result.push(createWeek(arr[i]));
	}

	return result;

	//return a 2d array
};

exports.deleteCrse = (arr, crse, subj) => {
	let output = [];

	for (let i = 0; i < arr.length; i++) {
		if (crse === arr[i][0].crse && subj === arr[i][0].subj) {
			continue;
		} else {
			output.push(arr[i]);
		}
	}

	return output;
};

//Takes the date string & changes the format so we can pass the correct
//identifier through our RRules

// //@params -
// //dates - the string of dates to check

// //@returns - an array representing the dates to enter into the RRule
// const createDates = (dates) => {
// 	let tmpDate = [];
// 	// console.log(dates);
// 	for (let i = 0; i < dates.length; i++) {
// 		if (dates.charAt(i) === "M") {
// 			tmpDate.push(0);
// 		} else if (dates.charAt(i) === "T") {
// 			tmpDate.push(1);
// 		} else if (dates.charAt(i) === "W") {
// 			tmpDate.push(2);
// 		} else if (dates.charAt(i) === "R") {
// 			tmpDate.push(3);
// 		} else if (dates.charAt(i) === "F") {
// 			tmpDate.push(4);
// 		} else if (dates.charAt(i) === "S") {
// 			tmpDate.push(5);
// 		}
// 	}
// 	// console.log(tmpDate);
// 	return tmpDate;
// };

//Another helper function to help RRule.  This creates an array of all times
//within the individual object for the required dates and start times

// //@params -
// //date: the date object returned from getDates
// //obj: the individual course object

// //@returns -
// //An array of Date objects to be used in the calendar

// const startTimes = (obj) => {
// 	let startRule = new RRule({
// 		freq: RRule.WEEKLY,
// 		interval: 1,
// 		byweekday: createDates(obj.days),
// 		dtstart: new Date(
// 			2020,
// 			0,
// 			6,
// 			parseInt(obj.startTime.substring(2)),
// 			parseInt(obj.startTime.substring(0, 2))
// 		),
// 		until: new Date(2020, 3, 3),
// 	});
// 	console.log("Start Hours:" + obj.startTime.substring(0, 2));
// 	console.log("Start Minutes: " + obj.startTime.substring(2));

// 	return startRule.all();
// };

// //Another helper function to help RRule.  This creates an array of all times
// //within the individual object for the required dates and end times

// //@params -
// //date: the date object returned from getDates
// //obj: the individual course object

// //@returns -
// //An array of Date objects to be used in the calendar

// //honestly, now I think about this, we can probably just use one function for this
// //it's doing the same shit.  Whatever, it's here now.

// const endTimes = (obj) => {
// 	let endRule = new RRule({
// 		freq: RRule.WEEKLY,
// 		interval: 1,
// 		byweekday: createDates(obj.days),
// 		dtstart: new Date(
// 			2020,
// 			0,
// 			6,
// 			parseInt(obj.startTime.substring(0, 2)),
// 			parseInt(obj.startTime.substring(2))
// 		),
// 		until: new Date(2020, 3, 3),
// 	});
// 	// console.log("endTime Hours:" + obj.endTime.substring(0, 2));
// 	// console.log("endTime Minutes: " + obj.endTime.substring(2));

// 	return endRule.all();
// };

// //This is a function to be finally used in the createCalendarObj method below
// //It takes all the courses in an individual schedule and creates time objects
// //to be used in the Calendar.

// //@params -
// //arr - each individual instance of a conflict free schedule option

// //@returns -
// //all the start times for every single instance of this "Schedule"
// //as an Array of time objects required by Calendar
// const findTimes = (arr) => {
// 	let result = [];
// 	let startArr = [];
// 	let endArr = [];

// 	// console.log(arr);
// 	for (let i = 0; i < arr.length; i++) {
// 		startArr = [];
// 		endArr = [];
// 		// console.log(arr[i])
// 		if (
// 			arr[i].type !== "WWW" &&
// 			arr[i].room !== "WWW" &&
// 			arr[i].room !== "OFFC"
// 		) {
// 			let dates = createDates(arr[i].days);
// 			// console.log(createDates(arr[i].days));
// 			// console.log(RRule.SU);
// 			// console.log(arr[i]);
// 			// console.log(startArr)
// 			// console.log(endArr)
// 			//get dates & times for end times

// 			let startDate = new Date(
// 				2020,
// 				0,
// 				6,
// 				parseInt(arr[i].startTime.substring(0, 2)),
// 				parseInt(arr[i].startTime.substring(2))
// 			);
// 			// console.log(startDate);
// 			// console.log(startDate.toISOString());
// 			// console.log(startDate.toDateString());
// 			// console.log(startDate.toString());
// 			// console.log(Date.parse(startDate));
// 			let endDate = new Date(
// 				2020,
// 				0,
// 				6,
// 				parseInt(arr[i].endTime.substring(0, 2)),
// 				parseInt(arr[i].endTime.substring(2))
// 			);

// 			let untilDate = new Date(2020, 3, 3);

// 			startArr = new RRule({
// 				freq: RRule.WEEKLY,
// 				interval: 1,
// 				wkst: 0,
// 				byweekday: createDates(arr[i].days),
// 				dtstart: startDate,
// 				until: untilDate,
// 			}).all();

// 			endArr = new RRule({
// 				freq: RRule.WEEKLY,
// 				interval: 1,
// 				wkst: 0,
// 				byweekday: createDates(arr[i].days),
// 				dtstart: endDate,
// 				until: untilDate,
// 			}).all();

// 			// console.log(startArr[0]);
// 			// console.log(endArr[0]);

// 			// console.log(arr[i]);
// 			// console.log(startArr);
// 			// console.log(endArr);

// 			//take start + end array, and create time instances for the calendar
// 			//theoretically startArr & endArr should be of the same length

// 			for (let j = 0; j < startArr.length; j++) {
// 				let calendarObj = {
// 					id: i,
// 					title: `${arr[i].subj} ${arr[i].crse}`,
// 					start: startArr[j],
// 					end: endArr[j],
// 					desc: arr[i].descr,
// 				};
// 				result.push(calendarObj);
// 			}
// 			// console.log(result);
// 		}
// 	}
// 	return result;
// };

// //This is the event controller for creating calendar objects

// //@params -
// //arr - The array of NON-CONFLICTING course schedules
// //In the calendar this would either be the prop or state that when given the
// //searchedCourses array to the non-conflict has produced all the non-conflicting
// //TLDR; Pass it this.state.noConflicts[i] or this.prop.noConflicts[i], or whatever it's called

// //@returns -
// //ALL of the date/times for the calendar, so when you switch from one no-conflict schedule
// //to the next, it updates.  The result[i] will be every single time slot for the
// //current schedule the user is on

// exports.createCalendarObj = (arr) => {
// 	let result = [];

// 	for (let i = 0; i < arr.length; i++) {
// 		result.push(findTimes(arr[i]));
// 	}

// 	return result;

// 	//return a 2d array
// };
