import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import React from 'react';
import ReactDOM from 'react-dom';
import ExportedApp from './Application.js';
import './styles/style.css';
import './i18n';

ReactDOM.render(
  <ExportedApp />,
  document.getElementById('app')
);