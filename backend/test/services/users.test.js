const app = require('../../src/app')

describe('\'users\' service', () => {
  it('registered the service', () => {
    const service = app.service('users')
    expect(service).toBeTruthy()
  })
  
  test("Get user", ()=>{
    const service = app.service('users');
    
    let userParams = {
      username: "user09",
      email:"user@user.com",
      password:"$User5131"
    }
    
    service.get("5e90cd722cfb0d3169cfdd1a", userParams)
    .then(result=>{
      expect(result.username).toBe("user09");
    })
    .catch(error=>console.log(error));
  })
})

// test("Get user", ()=>{
//   let userName;
  
//   app.service('users').get(/*fill data here*/)
//   .then(result=>{
//     userName = result.userName
//   })
//   .catch(error=>console.log(error));
  
//   expect(userName).toBe("name here");
// })
