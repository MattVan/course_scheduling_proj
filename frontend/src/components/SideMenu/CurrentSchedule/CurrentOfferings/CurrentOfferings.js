import React, { Component } from "react";
import MyModal from "../../../UI/Modal";

export default class CurrentOfferings extends Component {
	//from props: currentSchedule, currentIndex, noConflictSchedules

	constructor(props) {
		super(props);

		this.renderTable = this.renderTable.bind(this);
		this.findCrns = this.findCrns.bind(this);
		this.findCourses = this.findCourses.bind(this);
		this.filterArray = this.filterArray.bind(this);
	}

	//in props have
	//currentIndex
	//noConflictSchedules array
	//courses array

	//isolate CRN's from noConflictSchedules[currentIndex]
	//pull from courses the crn's of the specific courses

	//use those specific courses [{crse1}, {crse2}] to render the table

	//pass through here noConflicts[currentIndex]
	findCrns(arr) {
		if (arr !== undefined) {
			//first object`
			let result = [arr[0].crn];
			let pastCrn = arr[0].crn;
			for (let i = 1; i < arr.length; i++) {
				if (arr[i].crn !== pastCrn) {
					result.push(arr[i].crn);
					pastCrn = arr[i].crn;
				}
			}
			return result;
		} else {
			return [];
		}
	}

	//pass through here the currentSchedules prop
	//& crnArray from above function
	findCourses(searchedCourses, crnArray) {
		let curr;
		let result = [];
		if (searchedCourses.length !== 0) {
			for (let i = 0; i < crnArray.length; i++) {
				curr = crnArray[i];
				for (let j = 0; j < searchedCourses.length; j++) {
					for (let k = 0; k < searchedCourses[j].length; k++) {
						if (searchedCourses[j][k].crn === curr) {
							result.push(searchedCourses[j][k]);
							break;
						}
					}
				}
			}
		}
		return result;
	}

	//get rid of non-standard start/endDate
	//parameter will be the array returned from findCOurses
	filterArray(arr) {
		//type, days, times, start time, end time, room, instructor
		if (arr === undefined) {
			return [];
		} else {
			for (let i = 0; i < arr.length; i++) {
				let newTimes = [];

				for (let j = 0; j < arr[i].times.length; j++) {
					if (j % 7 !== 3 || j % 7 !== 4) {
						newTimes.push(arr[i].times[j]);
					}
				}
				arr[i].times = newTimes;
			}

			return arr;
		}
	}

	// <thead>
	// 	<th scope="col">Type</th>
	// 	<th scope="col">Days</th>
	// 	<th scope="col">Time</th>
	// 	<th scope="col">Room</th>
	// 	<th scope="col">Instructor</th>
	// </thead>

	renderTable(obj, t) {
		let key = 1;
		return [
			<h2 key={key++}>{obj.title}</h2>,
			<p key={key++}>
				{t("currentOfferingsModal.tableHeadings.subjectCourse")}:{" "}
				{obj.subj + obj.crse}
			</p>,
			<p key={key++}>
				{t("currentOfferingsModal.tableHeadings.crn")}: {obj.crn}
			</p>,
			<p key={key++}>
				{t("currentOfferingsModal.tableHeadings.credits")}: {obj.cred}
			</p>,
			<p key={key++}>
				{t("currentOfferingsModal.tableHeadings.section")}: {obj.sec}
			</p>,

			<table key={key++}>
				<tbody key={key++}>
					<tr key={key++}>
					
						{obj.times.map((data) => {
							return <td key={++key}>{data}</td>;
						})}
					</tr>
				</tbody>
			</table>,
		];
	}

	render() {
		let crnArray = this.findCrns(
			this.props.nonConflictSchedules[this.props.currentIndex]
		);

		let courses = this.findCourses(this.props.currentSchedule, crnArray);

		let filteredArr = this.filterArray(courses);

		const { t } = this.props;
		// console.log(crnArray);
		// console.log(courses);
		// console.log(filteredArr);

		return (
			<MyModal
				show={this.props.showModal}
				size={"lg"}
				title={t("currentOfferingsModal.title")}
				close={this.props.closeModal}
			>
				<div key={Math.random() * 10000}>
					{filteredArr.map((data) => {
						return this.renderTable(data, t);
					})}
				</div>
			</MyModal>
		);
	}
}
