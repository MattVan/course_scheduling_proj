export default [
    {
        courseNumber: '11822',
        course: '2650',
        title: 'Full Stack Web Development I',
        days: '-----S-',
        time:   [
                '0830-1020',
                '1020-1220',
        ],
        room:   [
                    'A275',
                    'A275',
        ],
        instructor: 'Jordan Miller'
    },
    {
        courseNumber: '10670',
        course: '2600',
        title: 'Full Stack Web Development II',
        days: '-----S-',
        time:   [
                '0930-1020',
                '1030-1120',
                '1330-1420',
                '1430-1520',
                '1130-1220',
                '1530-1620'
        ],
        room:   [
                    'B023',
                    'B019',
                    'B023',
                    'B019',
                    'B019',
                    'B019'
        ],
        instructor: 'Brian Koehler'
    },
    {
        courseNumber: '21124',
        course: '1280',
        title: 'Unix Tools and Scripting',
        days: "'---R---','M-W----'",
        time:   [
                '1430-1620',
                '1430-1620',
        ],
        room:   [
                    'WWW',
                    'WWW',
        ],
        instructor: 'Kim Lam'
    },
    {
        courseNumber: '21124',
        course: '1280',
        title: 'Scripting',
        days: "'---R---','M-W----'",
        time:   [
                '1430-1620',
                '1430-1620',
        ],
        room:   [
                    'WWW',
                    'WWW',
        ],
        instructor: 'Kim Lam'
    },
    {
        courseNumber: '21124',
        course: '1280',
        title: 'Tools and Scripting',
        days: "'---R---','M-W----'",
        time:   [
                '1430-1620',
                '1430-1620',
        ],
        room:   [
                    'WWW',
                    'WWW',
        ],
        instructor: 'Kim Lam'
    },
    {
        courseNumber: '21124',
        course: '1280',
        title: 'Tools',
        days: "'---R---','M-W----'",
        time:   [
                '1430-1620',
                '1430-1620',
        ],
        room:   [
                    'WWW',
                    'WWW',
        ],
        instructor: 'Kim Lam'
    },

]