const { authenticate } = require('@feathersjs/authentication').hooks;

const processSchedule = require('../../hooks/process-schedule');

module.exports = {
  before: {
    all: [ authenticate('jwt') ], 
    find: [],
    get: [],
    create: [processSchedule()],
    update: [processSchedule()],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
