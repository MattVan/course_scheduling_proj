const mongoose = require('mongoose')
const logger = require('./logger')

module.exports = function (app) {
  mongoose.connect(
    app.get('mongodb'),
      { useCreateIndex: true, useNewUrlParser: true }
    )
    // added then block to ensure connection
    .then( () => { console.log('connected');})
    .catch(err => {
    // connection failure
      console.log('error connecting');
      logger.error(err)
        process.exit(1)
    })

  mongoose.Promise = global.Promise

  app.set('mongooseClient', mongoose)
}
