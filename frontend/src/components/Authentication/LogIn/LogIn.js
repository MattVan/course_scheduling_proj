import React from "react";

const LogIn = props => {
	const { t } = props;
	return (
		<div>
			<form onSubmit={props.login}>
				<div className="form-group">
					<label htmlFor="Email">{t('authentication.modal.tabs.loginForm.email')}</label>
					<input
						type="text"
						name="email"
						className="form-control"
						id="email"
						placeholder={t('authentication.modal.tabs.loginForm.emailPlaceholder')}
						required
					/>
				</div>
				<div className="form-group">
					<label htmlFor="password">{t('authentication.modal.tabs.loginForm.password')}</label>
					<input
						name="password"
						type="password"
						className="form-control"
						id="password"
						placeholder={t('authentication.modal.tabs.loginForm.passwordPlaceholder')}
						required
					/>
				</div>
				<button type="submit" className="btn btn-primary btn-block">
					{t('authentication.modal.tabs.loginForm.submitButton')}
				</button>
			</form>
		</div>
	);
};

export default LogIn
