#!/bin/sh
chmod -R o+w $PWD/backend $PWD/frontend

docker run --rm $1 --network scheduler-net --name frontend \
    -v $PWD/frontend:/mnt/frontend \
    -w /mnt/frontend  bkoehler/feathersjs npm run firststart
    
sleep 120

docker run --rm $1 --network scheduler-net --name backend \
    -v $PWD/backend:/mnt/backend \
    -w /mnt/backend  bkoehler/feathersjs npm run firststart
    
sleep 60


#!/bin/sh
docker run --network scheduler-net --rm $1 --name scheduler-nginx \
	 -v $PWD/etc/nginx/conf.d:/etc/nginx/conf.d \
	 -v /home/ec2-user/etc/letsencrypt:/etc/letsencrypt \
	 -v /home/ec2-user/var/lib/letsencrypt:/var/lib/letsencrypt \
	 -p 80:80 -p 443:443 \
	 nginx:alpine