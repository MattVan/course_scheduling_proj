// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {
    
    console.log('processing schedule');
    const { data } = context;
    
    console.log(data);
    
    const courses = data.currentSchedule;
    const userId = data.userId;
    const scheduleName = data.scheduleName;
    
    if(!courses){
      throw new Error("Courses are required!");
    }
    if(!userId){
      throw new Error("User ID is required!");
    }
    
    if(!scheduleName){
      throw new Error("Schedule Name is required!");
    }
    
    if(courses.length < 1 || courses.length > 5){
      throw new Error("Must have more than 1 courses, but less than 6");
    }
    
    console.log("Processed Schedule Successfully");
    
    return context;
    
  };
};
