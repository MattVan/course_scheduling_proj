import React, { Component, Suspense } from "react";
import {
	BrowserRouter as Router,
	Switch,
	Route,
	NavLink,
	Redirect,
} from "react-router-dom";

import { toast } from "react-toastify";

import { withTranslation } from "react-i18next";

import client from "./feathers";
import Spinner from "react-bootstrap/Spinner";
import "react-toastify/dist/ReactToastify.css";
import Calendar from "./components/Calendar/Calendar";
import SideMenu from "./components/SideMenu/SideMenu";

import {
	findCourses,
	nonConflictingCourses,
	createCalendarObj,
	deleteCrse,
} from "./utils/Course";

toast.configure({
	autoClose: 2000,
	draggable: false,
});

export class Application extends Component {
	constructor(props) {
		super(props);
		this.state = {
			currentSchedule: [],
			userSchedules: [],
			nonConflictSchedules: [],
			user: null,
			searchFormFeedback: "",
			searchFormClass: "",
			events: null,
			currentIndex: 0,
		};

		this.loadSchedule = this.loadSchedule.bind(this);
		this.deleteSchedule = this.deleteSchedule.bind(this);
		this.login = this.login.bind(this);
		this.logout = this.logout.bind(this);
		this.courseSearch = this.courseSearch.bind(this);
		this.previousIndex = this.previousIndex.bind(this);
		this.nextIndex = this.nextIndex.bind(this);
		this.saveSchedule = this.saveSchedule.bind(this);
		this.deleteCourse = this.deleteCourse.bind(this);
	}

	componentDidMount() {
		const userService = client.service("users");
		const schedulesService = client.service("schedules");
		const coursesService = client.service("get-courses");
		const { t } = this.props;

		this.setState({
			userService,
			schedulesService,
			t,
		});

		coursesService
			.get({})
			.then((results) => {
				this.setState({
					courses: results,
				});
			})
			.catch((e) => console.log(e));
		// Attempt to retrieve token from local storage and authenticate
		client
			.reAuthenticate()
			.then((response) => {
				// console.table(response);
			})
			.catch((error) => {
				// console.log(error);
			});

		// Set user and schedules to state on authentication
		client.on("authenticated", (response) => {
			// Find user schedules
			schedulesService
				.find({
					query: {
						// $limit: 5,
						userId: response.user._id,
					},
				})
				// update the state with user and  user schedules
				.then((results) => {
					this.setState({
						user: response.user,
						userSchedules: results.data,
					});
				})
				.catch((error) => {
					console.log(error);
				});
		});

		schedulesService.on("created", (schedule) => {
			const userSchedules = this.state.userSchedules;
			userSchedules.push(schedule);
			this.setState({
				userSchedules,
			});
		});
	}

	deleteSchedule(scheduleId) {
		const schedulesService = this.state.schedulesService;
		let userSchedules = this.state.userSchedules;
		let newSchedules = userSchedules.filter(
			(schedule) => schedule._id !== scheduleId
		);

		schedulesService
			.remove(scheduleId)
			.then((response) => {
				this.setState({
					userSchedules: newSchedules,
				});
				toast.success(this.state.t("toast.scheduleDeleteSuccess"));
			})
			.catch((error) => {
				console.log("Error: " + error);
				toast.error(this.state.t("toast.scheduleDeleteError"));
			});
	}

	login(event) {
		event.preventDefault();
		const email = event.target.email.value;
		const password = event.target.password.value;
		client
			.authenticate({
				strategy: "local",
				email: email,
				password: password,
			})
			.then((result) => {
				console.log(result);
			})
			.catch((error) => {
				console.log(error);
			});
	}

	logout() {
		client.logout();
		this.setState({
			currentSchedule: [],
			userSchedules: [],
			nonConflictSchedules: [],
			user: null,
			searchFormFeedback: "",
			searchFormClass: "",
			events: null,
			currentIndex: 0,
		});
	}

	courseSearch(event) {
		event.preventDefault();

		const courseNumber = event.target.courseNumber.value;
		const courseSubject = event.target.courseSubject.value;
		const courses = this.state.courses;
		const currentSchedule = this.state.currentSchedule || [];
		let results = findCourses(
			courseSubject,
			courseNumber,
			currentSchedule,
			courses
		);
		// Search for courses under given parameters;
		if (results != undefined) {
			// Push the course into current schedule && get non conflicting schedules
			currentSchedule.push(results);
			let nonConflictSchedules = nonConflictingCourses(currentSchedule);
			if (
				this.state.nonConflictSchedules.length ===
					nonConflictSchedules.length &&
				this.state.nonConflictSchedules[0].length ===
					nonConflictSchedules[0].length
			) {
				console.log("inside if");
				currentSchedule.pop();
				//don't update anything, but print an error message
				this.setState({
					searchFormClass: "is-invalid",
					searchFormFeedback: this.state.t("formLabels.searchFormConflicts"),
				});
			} else if (nonConflictSchedules.length !== 0) {
				console.log("inside else if");
				let calendarObj = createCalendarObj(nonConflictSchedules);
				this.setState({
					currentSchedule: currentSchedule,
					nonConflictSchedules: nonConflictSchedules,
					events: calendarObj,
					searchFormClass: "",
					searchFormFeedback: "",
				});
			}
		} else {
			console.log("else");
			this.setState({
				searchFormClass: "is-invalid",
				searchFormFeedback: this.state.t("formLabels.searchFormNoResults"),
			});
		}
	}

	previousIndex() {
		let newIndex = this.state.currentIndex - 1;
		if (newIndex < 0) return;
		this.setState({
			currentIndex: newIndex,
		});
	}

	nextIndex() {
		let newIndex = this.state.currentIndex + 1;
		if (newIndex >= this.state.events.length) return;
		this.setState({
			currentIndex: newIndex,
		});
	}

	saveSchedule(event) {
		event.preventDefault();

		const scheduleName = event.target.scheduleName.value;

		const userId = this.state.user._id;
		const scheduleService = this.state.schedulesService;
		const currentSchedule = this.state.currentSchedule;
		const currentIndex = this.state.currentIndex;

		scheduleService
			.create({
				userId: userId,
				currentSchedule: currentSchedule,
				currentIndex: currentIndex,
				scheduleName: scheduleName,
			})
			.then((results) => {
				toast.success(
					this.state.t("toast.scheduleSaveSuccess") +
						": " +
						results.scheduleName
				);
			})
			.catch((error) => {
				toast.error(this.state.t("toast.scheduleSaveError"));
				console.log(error);
			});
	}

	loadSchedule(scheduleId) {
		const scheduleService = this.state.schedulesService;
		scheduleService
			.get(scheduleId, {})
			.then((result) => {
				let nonConflictSchedules = nonConflictingCourses(
					result.currentSchedule
				);
				let calendarObj = createCalendarObj(nonConflictSchedules);

				this.setState({
					currentSchedule: result.currentSchedule,
					nonConflictSchedules: nonConflictSchedules,
					events: calendarObj,
					currentIndex: result.currentIndex,
				});
				toast.success(
					this.state.t("toast.scheduleLoadSuccess") + ": " + result.scheduleName
				);
			})
			.catch((error) => {
				console.log("Error: " + error);
				toast.error(this.state.t("toast.scheduleLoadError"));
			});
	}

	deleteCourse(crse) {
		let currentSchedule = deleteCrse(
			this.state.currentSchedule,
			crse[0].crse,
			crse[0].subj
		);

		let nonConflictingSchedules = nonConflictingCourses(currentSchedule);
		let events = createCalendarObj(nonConflictingSchedules);

		if (currentSchedule.length === 0) {
			currentSchedule = null;
			events = null;
		}

		this.setState({
			currentSchedule: currentSchedule,
			nonConflictSchedules: nonConflictingSchedules,
			events: events,
		});
	}

	render() {
		const { i18n } = this.props;
		const { t } = this.props;

		function changeLanguage(lng) {
			i18n.changeLanguage(lng);
		}

		return (
			<Suspense fallback={<Loader />}>
				<Router>
					<div>
						<h1 className="ml-3 mt-3">{t("title")}</h1>
					</div>
					<nav className="navbar d-flex justify-content-start bg-light m-2">
						<NavLink
							className="nav-item mr-2"
							activeClassName="nav-item active"
							to="/"
							onClick={() => changeLanguage("en")}
						>
							English
						</NavLink>
						<NavLink
							className="nav-item mr-2"
							to="/es/scheduler"
							onClick={() => changeLanguage("es")}
						>
							Spanish
						</NavLink>
						<NavLink
							className="nav-item mr-2"
							activeClassName="nav-item active"
							to="/zh/scheduler"
							onClick={() => changeLanguage("zh")}
						>
							Chinese
						</NavLink>
					</nav>
					<Switch>
						<Redirect exact from="/" to="/en/scheduler" />
						<Route path="/en/scheduler">
							<div className="container">
								<div className="row">
									<div className="col-3 h-100">
										<SideMenu
											user={this.state.user}
											currentSchedule={this.state.currentSchedule}
											nonConflictSchedules={this.state.nonConflictSchedules}
											currentIndex={this.state.currentIndex}
											previousIndex={this.previousIndex}
											nextIndex={this.nextIndex}
											saveSchedule={this.saveSchedule}
											userSchedules={this.state.userSchedules}
											t={t}
											userService={this.state.userService}
											login={this.login}
											loadSchedule={this.loadSchedule}
											deleteSchedule={this.deleteSchedule}
											courseSearch={this.courseSearch}
											searchFormFeedback={this.state.searchFormFeedback}
											searchFormClass={this.state.searchFormClass}
											logout={this.logout}
											deleteCourse={this.deleteCourse}
										/>
									</div>
									<div className="col-9">
										<Calendar
											schedules={
												this.state.events === null
													? []
													: this.state.events[this.state.currentIndex]
											}
											locale={"en"}
										/>
									</div>
								</div>
							</div>
						</Route>
						<Route path="/es/scheduler">
							<div className="container">
								<div className="row">
									<div className="col-3">
										<SideMenu
											user={this.state.user}
											currentSchedule={this.state.currentSchedule}
											nonConflictSchedules={this.state.nonConflictSchedules}
											currentIndex={this.state.currentIndex}
											previousIndex={this.previousIndex}
											nextIndex={this.nextIndex}
											saveSchedule={this.saveSchedule}
											userSchedules={this.state.userSchedules}
											t={t}
											userService={this.state.userService}
											login={this.login}
											loadSchedule={this.loadSchedule}
											deleteSchedule={this.deleteSchedule}
											courseSearch={this.courseSearch}
											searchFormFeedback={this.state.searchFormFeedback}
											searchFormClass={this.state.searchFormClass}
											logout={this.logout}
											deleteCourse={this.deleteCourse}
										/>
									</div>
									<div className="col-9">
										<Calendar
											schedules={
												this.state.events === null
													? []
													: this.state.events[this.state.currentIndex]
											}
											locale={"es"}
										/>
									</div>
								</div>
							</div>
						</Route>
						<Route path="/zh/scheduler">
							<div className="container">
								<div className="row">
									<div className="col-3">
										<SideMenu
											user={this.state.user}
											currentSchedule={this.state.currentSchedule}
											nonConflictSchedules={this.state.nonConflictSchedules}
											currentIndex={this.state.currentIndex}
											previousIndex={this.previousIndex}
											nextIndex={this.nextIndex}
											saveSchedule={this.saveSchedule}
											userSchedules={this.state.userSchedules}
											t={t}
											userService={this.state.userService}
											login={this.login}
											loadSchedule={this.loadSchedule}
											deleteSchedule={this.deleteSchedule}
											courseSearch={this.courseSearch}
											searchFormFeedback={this.state.searchFormFeedback}
											searchFormClass={this.state.searchFormClass}
											logout={this.logout}
											deleteCourse={this.deleteCourse}
										/>
									</div>
									<div className="col-9">
										<Calendar
											schedules={
												this.state.events === null
													? []
													: this.state.events[this.state.currentIndex]
											}
											locale={"zh-cn"}
										/>
									</div>
								</div>
							</div>
						</Route>
					</Switch>
				</Router>
			</Suspense>
		);
	}
}

const ExportApp = withTranslation()(Application);

const Loader = () => (
	<div className="d-flex flex-column align-items-center">
		<Spinner animation="border" variant="primary" />
		{/* <div>Loading...</div> */}
	</div>
);

export default function ExportedApp() {
	return (
		<Suspense fallback={<Loader />}>
			<ExportApp />
		</Suspense>
	);
}
