const app = require('../../src/app');

describe('\'getCourses\' service', () => {
  it('registered the service', () => {
    const service = app.service('get-courses');
    expect(service).toBeTruthy();
  });
  
  
  test("Array Length", ()=>{
    const service = app.service('get-courses');
  
    service.get({})
    .then(result=>{
      expect(result).toHaveLength(1271);
    })
    .catch(error=>console.log(error));

  })
  
});
