import React from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import "react-big-calendar/lib/css/react-big-calendar.css";
import moment from "moment";
import "moment/locale/es";
import "moment/locale/zh-cn";

// Setup the localizer by providing the moment (or globalize) Object
// to the correct localizer.
moment.locale('en');
const localizer = momentLocalizer(moment);

const Event = ({ event }) => {
	return (
		<span style={{ height: "100%", fontSize: "12px", padding: "2px" }}>
			<strong>{event.title}</strong>
			<p className="p-2">{event.desc}</p>
		</span>
	);
};

const MyCalendar = (props) => {
	return (
		<Calendar
			defaultDate={new Date(2020, 0, 5)}
			localizer={localizer}
			events={props.schedules}
			views={{ week: true }}
			defaultView="week"
			showMultiDayTimes
			components={{
				event: Event,
			}}
			style={{ height: "90vh" }}
			toolbar={false}
			step={60}
			min={new Date(2020, 0, 5, 7, 0)}
			max={new Date(2020, 0, 5, 21, 30)}
		/>
	);
};

export default MyCalendar;
