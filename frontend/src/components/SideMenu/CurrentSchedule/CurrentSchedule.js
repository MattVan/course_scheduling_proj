import React, { Component } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import ListGroup from "react-bootstrap/ListGroup";

import CurrentOfferings from "./CurrentOfferings/CurrentOfferings";

import {
	FaLongArrowAltRight,
	FaLongArrowAltLeft,
	FaSave,
} from "react-icons/fa";

export default class CurrentSchedule extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showModal: false,
		};

		this.toggleModal = this.toggleModal.bind(this);
	}

	toggleModal() {
		this.setState({
			showModal: !this.state.showModal,
		});
	}

	render() {
		const { t } = this.props;

		let arrowsComponent = (
			<div className="d-flex flex-row justify-content-between mt-3">
				<button
					disabled={this.props.currentIndex - 1 < 0}
					onClick={this.props.previousIndex}
					className="btn btn-secondary"
				>
					<span>
						<FaLongArrowAltLeft />
					</span>{" "}
					{t('currentScheduleList.previousButton')}
				</button>
				<button
					disabled={
						this.props.nonConflictSchedules
							? this.props.currentIndex + 1 >=
							  this.props.nonConflictSchedules.length
							: false
					}
					onClick={this.props.nextIndex}
					className="btn btn-secondary"
				>
					{t("currentScheduleList.nextButton")}
					<span>
						<FaLongArrowAltRight />
					</span>
				</button>
			</div>
		);

		let saveScheduleComponent = (
			<form
				onSubmit={this.props.saveSchedule}
				className="d-flex flex-row justify-content-between align-items-bottom mt-3"
			>
				<div>
				<label htmlFor="scheduleName">{t("currentScheduleList.scheduleName")}</label>
					<input
						type="text"
						className="form-control"
						id="scheduleName"
						placeholder={t("currentScheduleList.scheduleNamePlaceholder")}
						name={"scheduleName"}
					/>
				</div>
				<button
					type="submit"
					disabled={!this.props.user && this.props.nonConflictSchedules === null || this.props.nonConflictSchedules.length === 0}
					className="btn btn-success h-50 align-self-end"
				>
					{t('currentScheduleList.saveButton')}{" "}
					<span>
						<FaSave />
					</span>
				</button>
			</form>
		);

		let text = <span>{t("currentScheduleList.noCoursesMessage")}</span>;
		let container;
		let courseList;
		if (this.props.currentSchedule.length > 0) {
			courseList = (
				<div className="container w-100">
					<h6>{t("currentScheduleList.courseListTitle")}:</h6>
					<ListGroup variant="flush" className="w-100">
						{
							this.props.currentSchedule.map((course) => {
								return (
									<ListGroup.Item className="w-100 d-flex align-items-center justify-content-between mt-2" key={course[0].title}>
										{course[0].title}
								<button onClick={() => {this.props.deleteCourse(course)}} className="btn btn-danger">{t('currentScheduleList.deleteCourseButton')}</button>
									</ListGroup.Item>
								);
							})
						}
					</ListGroup>
				</div>
			);
			container = (
				<Container className="overflow-auto sidemenu">
					<CurrentOfferings
						showModal={this.state.showModal}
						closeModal={this.toggleModal}
						currentSchedule={this.props.currentSchedule}
						nonConflictSchedules={this.props.nonConflictSchedules}
						currentIndex={this.props.currentIndex}
						t={t}
					/>
					<Row>{courseList}</Row>
					<div className="d-flex flex-row justify-content-between align-items-center mt-5">
						<span>
							{t('currentScheduleList.schedule')}: [{this.props.nonConflictSchedules.length  === 0 ? 0 : this.props.currentIndex + 1}/
							{this.props.nonConflictSchedules.length}]
						</span>
						<button onClick={this.toggleModal} className="btn btn-primary">
							{t('currentScheduleList.currentOfferingsButton')}
						</button>
					</div>
					{this.props.currentSchedule ? arrowsComponent : null}
					{this.props.user ? saveScheduleComponent : null}
				</Container>
			);
		}
		return (
			<div className="mt-3">
				<h4>{t("currentScheduleList.title")}</h4>
				{this.props.currentSchedule.length === 0 && text}
				{this.props.currentSchedule && container}
			</div>
		);
	}
}
